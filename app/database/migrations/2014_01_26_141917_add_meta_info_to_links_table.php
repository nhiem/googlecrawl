<?php

use Illuminate\Database\Migrations\Migration;

class AddMetaInfoToLinksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
            Schema::table('links', function($table)
		{
			//
                    $table->string('meta_info')->after('url');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}